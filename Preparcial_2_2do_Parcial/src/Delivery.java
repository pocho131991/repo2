import java.util.Random;

public class Delivery extends Thread implements Atendible{

	private Mesera mesera;
	
	public Delivery(Mesera mesera) {
		super();
		this.mesera = mesera;
	}

	@Override
	public Pedido generarPedido() {
		// TODO Auto-generated method stub
		return new Pedido("Delivery", " un pedido de comida");
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		Integer tope = rnd.nextInt(5)+1;
		for (int i = 0; i < tope; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
	
		}
		this.getMesera().tomarOrden(null);//getListaPedidos().add(null);
		
	
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}
	
	
}
