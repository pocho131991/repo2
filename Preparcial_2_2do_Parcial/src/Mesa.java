import java.util.Random;

public class Mesa implements Atendible,Runnable{

	private Integer nro;
	private Mesera mesera;
	
	
	public Mesa(Mesera mesera, Integer nro) {
		super();
		this.mesera = mesera;
		this.setNro(nro);
	}

	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < new Random().nextInt(8)+1; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
		
		}
		this.getMesera().tomarOrden(null);//getListaPedidos().add(null);
	}

	@Override
	public Pedido generarPedido() {
		// TODO Auto-generated method stub
		return new Pedido("Mesa "+ this.getNro().toString(), " un pedido de comida");
	}


	public Mesera getMesera() {
		return mesera;
	}


	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}


	public Integer getNro() {
		return nro;
	}


	public void setNro(Integer nro) {
		this.nro = nro;
	}

	
	
}
