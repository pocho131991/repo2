
public class Pedido {
private String origen;
private String pedido;
public Pedido(String origen, String pedido) {

	this.origen = origen;
	this.pedido = pedido;
}
@Override
public String toString() {
	return "Pedido de : " + this.getOrigen() + ", Pedido: " + this.getPedido() ;
}

public String getOrigen() {
	return origen;
}
public void setOrigen(String origen) {
	this.origen = origen;
}
public String getPedido() {
	return pedido;
}
public void setPedido(String pedido) {
	this.pedido = pedido;
}


}
