
public class Cocina extends Thread {
	private Mesera mesera;

	public Cocina(Mesera mesera) {
		super();
		this.mesera = mesera;
	}

	private void prepararPedido(Pedido pedido) throws PerdidaDePedido {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (Math.random() > 0.80) {
			throw new PerdidaDePedido(pedido.getOrigen());
		} else {
			System.out.println("PREPANDO en cocina " + pedido.toString());
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Integer cantnull = 0;

		
			while (cantnull < 2) {
				Pedido pedido = this.getMesera().entregarACocina();
				if (pedido != null) {
					try {
						prepararPedido(pedido);	
					} catch (PerdidaDePedido e) {
						System.out.println(e.getMessage());
					}
					
				} else cantnull++;
					
				

			}
		
	
	System.out.println("Cierra la cocina");
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}

}
